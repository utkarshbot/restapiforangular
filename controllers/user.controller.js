const httpStatus=require('http-status');
const pick=require('../utils/pick');
const apiError=require('../utils/ApiError');
const catchAsync=require('../utils/catchAsync');
const {userService}=require('../services');

//creating user in the db
const createUser=catchAsync(async (req,res)=>{
    const user=userService.createUser(req.body);
    res.status(httpStatus.CREATED).send(user);
})

//incomplete function, paginate util fn need to created
const getUsers=catchAsync(async (req,res)=> {
    console.log("get all user pagination ")
    const filter = pick(req.query, ['name', 'role']);
    const options = pick(req.query, ['sortBy', 'limit', 'page']);
    console.log(filter);
    console.log(options);
    //here we will call the paginate fn
    const result = await userService.queryUsers(filter, options)
    res.send(result);
})
//get details of a single user
const getUser=catchAsync(async (req,res)=>{
    const user=await userService.getUserById(req.params.userId);
    if (!user){
        throw new apiError(httpStatus.NOT_FOUND,'User not found');
    }
    res.send(user);
})

//updating user details
const updateUser=catchAsync(async (req,res)=>{
    const user=userService.updateByUserId(req.params.userId,req.body);
    res.send(user);
})

//deleting user data
const deleteUser=catchAsync(async (req,res)=>{
    await userService.deleteUser(req.params.userId);
    res.status(httpStatus.NO_CONTENT).send('deleted user successfully')
})

module.exports={
    deleteUser,
    updateUser,
    getUser,
    getUsers,
    createUser
}