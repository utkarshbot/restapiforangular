const httpStatus = require('http-status');
const apiError=require('../utils/ApiError');
const userService=require('../services/user.service');
const tokenService=require('../services/token.service');
const Token=require('../models/token.model');
const {tokenTypes}=require('../config/tokens');

//Function to login the user with email and password
const loginUserWithEmailAndPassword=async (email,password)=>{
    const user=await userService.getUserByEmail(email);
    if (!user || !(await user.isPasswordMatch(password))){
        throw new apiError(httpStatus.UNAUTHORIZED,'Incorrect credentials');
    }
    return user;
}

//logout the user
const logout=async (refreshToken)=>{

}

//refresh auth token, with the help of the refresh token we give a new access token
const refreshAuth=async (refreshToken)=>{
    try {
        const refreshTokenDoc=await tokenService.verifyToken(refreshToken,tokenTypes.REFRESH);
        const user=await userService.getUserById(refreshTokenDoc.user);
        if (!user){
            throw new Error()
        }
        await refreshTokenDoc.remove();
        return tokenService.generateAuthToken(user);
    }catch (err){
        throw new apiError(httpStatus.UNAUTHORIZED,'Please authenticate')
    }
}

//reset password with token
const resetPassword=async (resetPasswordToken,newPassword)=>{
    try {
        console.log('password reset run')
        const resetPasswordTokenDoc=await tokenService.verifyToken(resetPasswordToken,tokenTypes.RESET_PASSWORD);
        console.log(resetPasswordToken);
        const user=await userService.getUserById(resetPasswordTokenDoc.user);
        if (!user){
            throw new Error();
        }
        console.log(user)
        await userService.updateByUserId(user.id,{password:newPassword});
        await Token.deleteMany({user:user.id,type:tokenTypes.RESET_PASSWORD});
    }catch (err){
        console.log(err)
        throw new apiError(httpStatus.UNAUTHORIZED,'Password reset failed');
    }
}

//verify email with the provided, and change status of user to verified
const verifyEmail=async (verifyEmailToken)=>{
    try {
        const verifyTokenDoc=await tokenService.verifyToken(verifyEmailToken,tokenTypes.VERIFY_EMAIL);
        const user=await userService.getUserById(verifyTokenDoc.user);
        if (!user){
            throw new Error();
        }
        await Token.deleteMany({user:user.id,type:tokenTypes.VERIFY_EMAIL});
        await userService.updateByUserId(user.id,{isEmailVerified:true});
    }catch (err){
        throw new apiError(httpStatus.UNAUTHORIZED,'Email verification failed')
    }
}

module.exports={
    resetPassword,
    loginUserWithEmailAndPassword,
    refreshAuth,
    verifyEmail
}