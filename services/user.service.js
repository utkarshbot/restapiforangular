const httpStatus = require('http-status');
const { User } = require('../models');
const ApiError = require('../utils/ApiError');

//create a user
const createUser=async (userBody)=>{
    if (await User.isEmailTaken(userBody.email))
        throw new ApiError(httpStatus.BAD_REQUEST,'Email already taken');
    return User.create(userBody);
}

//query user, returning according to the filter and options
const queryUsers = async (filter,options) => {
    const users = await User.paginate(filter, options);
    return users;
}
//get user by ID
const getUserById=async (id)=>{
    return User.findById(id);
}

//get user by email
const getUserByEmail=async (email)=>{
    return User.findOne({email});
}

//Update by UserID
const updateByUserId=async (userId,updateBody)=>{
    const user=await getUserById(userId);
    if (!user)
        throw new ApiError(httpStatus.NOT_FOUND,'User not found');
    //if there is a email and it has been taken already, throw a error
    if (updateBody.email && (await User.isEmailTaken(updateBody.email,userId))){
        throw new ApiError(httpStatus.BAD_REQUEST,'Email not available');
    }
    //adding the updated values to the Stored user and saving that state
    Object.assign(user,updateBody);
    await user.save();
    return user;
}
//delete user by id
const deleteUserById=async (userId)=>{
    const user=await getUserById(userId);
    if (!user)
        throw new ApiError(httpStatus.NOT_FOUND,'User not found');
    await user.remove();
    return user;
}

module.exports={
    deleteUserById,
    updateByUserId,
    getUserById,
    getUserByEmail,
    createUser,
    queryUsers
}
