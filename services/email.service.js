const sgMail=require('@sendgrid/mail');
const config=require('../config/config');

//registering the mail api key
sgMail.setApiKey(config.mail.apiKey);

//send mail fn to a receiver
const sendMail=async (to,subject,message)=>{
    let sendingMail = {
        to: to, // list of receivers
        from: "www.avinashkumar2@gmail.com", // sender address
        subject: `${subject} ⚡`, // Subject line
        text: subject, // plain text body
        html: `<h1>${message}</h1>`,
    };
    await sgMail.send(sendingMail);     //sending the mail to receiver
}

//sending the password reset mail to the user
const sendResetPasswordMail=async (receiver,resetPasswordToken)=>{
    let sendingMail = {
        to: receiver, // list of receivers
        from: "www.avinashkumar2@gmail.com", // sender address
        subject: `Password reset link ⚡`, // Subject line
        html: `<h1>Hi user, this is your password reset link, this link is valid for ${config.jwt.resetPasswordExpirationMinutes} minutes</h1>
               <h3>Link <a href="http://localhost:3000/reset-password?token=${resetPasswordToken}">Click here to reset password</a></h3>`,
    };
    await sgMail.send(sendingMail);     //sending the mail to receiver
}

//sending email verification link to user
const sendEmailVerificationLink=async (receiver,verificationToken)=>{
    let sendingMail = {
        to: receiver, // list of receivers
        from: "www.avinashkumar2@gmail.com", // sender address
        subject: `Email Verification link ⚡`, // Subject line
        html: `<h1>Hi user, this is your email verification link, this link is valid for ${config.jwt.verifyEmailExpirationMinutes} minutes</h1>
               <h3>Link <a href="http://localhost:3000/verify-email?token=${verificationToken}">Click here to verify email</a></h3>`,
    };
    await sgMail.send(sendingMail);     //sending the mail to receiver
}


module.exports={
    sendEmailVerificationLink,
    sendResetPasswordMail,
    sendMail
}