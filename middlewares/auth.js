const passport=require('passport');
const httpStatus=require('http-status');
const apiError=require('../utils/ApiError');
const {roleRights}=require('../config/roles');

//here we are making a wrapper fn , so that we can pass more required variables to the main fn,
//we are returning a fn (err,user,info) but as we know the fn is returned with its lexical scope bundled to it,
//therefore the variables we pass in the outer fn is accessible in the inside fn

const verifyCallback=(req,resolve,reject,requiredRights)=>(err,user,info)=>{
    if (err || !user){
        return reject(new apiError(httpStatus.UNAUTHORIZED,'Please authenticate'))
    }
    req.user=user;
    //according to the role of profile(user/admin), we are checking the provided rights to the that role,
    //if the rights required by the cur route triggered is ALL GRANTED to that role, then only we provide the required data
    if (requiredRights.length){
        const userRights=roleRights.get(user.role);     //getting the rights to that role
        const hasRequiredRights=requiredRights.every((requiredRights)=>userRights.includes(requiredRights));
        if (!hasRequiredRights && req.params.userId!==user.id){
            return reject(new apiError(httpStatus.FORBIDDEN,'Forbidden'))
        }
    }
    resolve();
}

const auth=(...requiredRights)=>async (req,res,next)=>{
    // console.log("Auth req rights");
    // console.log(req.headers.authorization)
    // console.log(req.params.userId)
    return new Promise((resolve,reject)=>{
        passport.authenticate('jwt',{session:false},verifyCallback(req,resolve,reject,requiredRights))(req,res,next)
    })
        .then(()=>next())
        .catch(err=>next(err));
};

module.exports=auth;